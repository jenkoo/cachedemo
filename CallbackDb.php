<?php

/**
 * Class Callback in db
 * 缓存的回调方法（无缓存，查询数据库）
 */

namespace Zcache;

class CallbackDbLib {

    private $db = null;

    public function __construct() {
        $this->db = get_slave_db();
    }

    public static function getInstance() {
        static $_instance = null;
        if (is_null($_instance)) {
            $_instance = new self();
        }
        return $_instance;
    }
    
    /**
     * 获取侵权商品 
     */
    public function QinQuanGoods(){
        return array(
            '陈萧寒',
            '马云',
            '马化腾'
        );
    }

    /**
     * 宠物小精灵专题活动
     */
    public function pokemon() {
        $data = array();
        $sql = 'SELECT pid,name,value,goods_img,cate_img,lev,num,weights,back_story,catch FROM ' . POKEMON;
        $res = $this->db->query($sql);
        $total = 0;
        while ($row = mysql_fetch_assoc($res)) {
            $data[$row['pid']] = $row;
            if ($row['num'] > 0)
                $total+=$row['weights'];
        }
        return array('total' => $total, 'data' => $data);
    }
}
