<?php
namespace Zcache;

class Memcache implements CacheInterface {

    private $memcache;

    private function _key($key) {
        return SHOP_CACHE_FILE_NAME . $key;
    }

    public function __construct() {
        $this->memcache = new \Memcache;
        $servers = unserialize(SHOP_MEMCACHE_HOSTS);
        foreach ($servers as $server) {
            $this->memcache->addServer($server['host'], $server['port'], SHOP_MEM_PCONNECT);
        }
    }

    public function set($key, $val, $expire = SHOP_MEM_EXPIRE, $flags = MEMCACHE_COMPRESSED) {
        if (!SHOP_CACHE_ENABLE) {
            return false;
        }
        return $this->memcache->set($this->_key($key), $val, $flags, $expire);
    }

    public function get($key) {
        if (!SHOP_CACHE_ENABLE) {
            return false;
        }
        return $this->memcache->get($this->_key($key));
    }

    public function mget($key = array()) {
        if (!SHOP_CACHE_ENABLE) {
            return false;
        }
        //得到对应关系
        $array = $desKey = array();
        $key = is_array($key) ? $key : explode(',', $key);
        foreach ($key as $k => $v) {
            $_k = $this->_key($v);
            $desKey[$k] = $_k;
            $array[$_k] = $v;
        }
        //返回对应关系
        $result = array();
        $valList = $this->memcache->get($desKey);
        if (is_array($valList) && $valList) {
            foreach ($valList as $k => $vlaue) {
                $result[$array[$k]] = $vlaue;
            }
        }
        return $result;
    }

    public function delete($key = '') {
        return $this->memcache->delete($this->_key($key));
    }

}
