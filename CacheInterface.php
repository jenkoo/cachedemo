<?php

/**
 * @author chenzhengkuo
 */
namespace Zcache;

interface CacheInterface {

    public function set($key, $val, $expire = 0);       //设置

    public function get($key);                          //获得单个值

    public function mget($keys);                       //获取多个值

    public function delete($key);                      //删除
}
