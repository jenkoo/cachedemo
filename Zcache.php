<?php

/**
 * Description of Zcache
 *
 * @author Jenkoo Chen
 */

namespace Zcache;

spl_autoload_register(
    function ($class) {
        $class = str_replace('\\', '/', $class);
        include_once ROOT_PATH . 'lib/' . $class . '.php';
    }
);

class Zcache {

    static protected $kindsConf;
    static protected $serverTypes;
    static protected $conf;

    static function getAllConf() {
        if (self::$kindsConf && self::$serverTypes) {
            return;
        }
        if (!Register::get('kindsConf') || !Register::get('serverTypes')) {
            require_once ROOT_PATH . 'lib/Zcache/Conf.php';
            Register::set('kindsConf', $kindsConf);
            Register::set('serverTypes', $serverTypes);
        }
        self::$kindsConf = Register::get('kindsConf');
        self::$serverTypes = Register::get('serverTypes');
    }
    
    static function getSingleConf($kind) {
        if (!array_key_exists($kind, self::$kindsConf)) {
            throw new \Exception('No match config!');
        }
        if (self::$kindsConf[$kind]['callback'] && !method_exists(self::getCallbackDbLib(), $kind)) {
            throw new \Exception('No match callback function!');
        }
        self::$conf = self::$kindsConf[$kind];
    }

    static function checkParam($kind, $id, $property1, $property2, $property3) {
        if (!$kind) {
            return false;
        }
        self::getAllConf();
        self::getSingleConf($kind);
        if (self::$conf['batch'] == CACHE_CONF_BATCH_TRUE && $id === false) {
            return false;
        }
        return true;
    }
    
    static function getKey($type, $id = false, $property1 = false, $property2 = false, $property3 = false) {
        $key = CACHE_PREFIX . ":";
        $key .=$type;
        if ($id) {
            $key .= ":" . $id;
        }
        if ($property1) {
            $key .= ":" . $property1;
        }
        if ($property2) {
            $key .= ":" . $property2;
        }
        if ($property3) {
            $key .= ":" . $property3;
        }
        return $key;
    }
    
    static private function getCacheServer() {
        $type = isset(self::$conf['server']) ? self::$conf['server'] : CACHE_CONF_SERVER_MEMCACHE;
        $server = array_key_exists($type, self::$serverTypes) ? self::$serverTypes[$type] : 'Memcache';
        if (!Register::get($server)) {
            switch ($server) {
                case 'Redis':
                    $cacheObj = new Redis();
                    break;
                default:
                    $cacheObj = new Memcache();
                    break;
            }
            Register::set($server, new $cacheObj);
        }
        return Register::get($server);
    }
    
    static private function getCallbackDbLib(){
        if (!Register::get('CallbackDbLib')) {
            Register::set('CallbackDbLib', new CallbackDbLib());
        }
        return Register::get('CallbackDbLib');
    }

    static function getInDb($kind, $id, $property1, $property2, $property3) {
       $dbObj = self::getCallbackDbLib();
       return $dbObj->$kind($id, $property1, $property2, $property3);
    }

    static function getCache($kind, $id = false, $property1 = false, $property2 = false, $property3 = false) {
        if (!self::checkParam($kind, $id, $property1, $property2, $property3)) {
            return false;
        }
        $key = self::getKey($kind, $id, $property1, $property2, $property3);
        if (self::$conf['frequent'] == CACHE_CONF_FREQUENT_TRUE && $res = Register::get($key)) {
            return $res;
        }
        $server = self::getCacheServer();
        if (!$res = $server->get($key)) {
            if(self::$conf['callback'] == CACHE_CONF_FREQUENT_TRUE && $res = self::getInDb($kind, $id, $property1, $property2, $property3)){
               $server->set($key,$res);
            }
        }
        if (self::$conf['frequent'] == CACHE_CONF_FREQUENT_TRUE) {
            Register::set($key,$res);
        }
        return $res;
    }
}
